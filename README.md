# Viola_Jones_Detection

This script is generalized repository for Viola & Jones (VJ) model training from positive and negative images

------------------------
This repository obtain the model from given positive and negative images. Repo is includen below files. 

| viola_jones_detection
| -- data
| -- info
| -- negImg
| -- negRawImg
| -- posImg
| -- resize_img.py
| -- run.sh

In order to run this repository, Opencv and Python should be installed

firstly, run.sh script. This script create above empty files.
After creating files, positive and negative images should put inside this empty folder (posImg folder have positive example image.)

positive image -> posImg
negative image -> negRawImg

Positive example image is 50x50 pixel, and negative images can be chosen any pixel greater than chosen pixel for positive image. 
For more information Opencv page can be examined. LINK : https://docs.opencv.org/2.4/doc/user_guide/ug_traincascade.html

After, Positives and negative images are put inside folder, run.sh script can run again and training process will be started for default training setting.

If You want to change any setting, You shuold chance run.sh file. For more information about Opencv Haar Cascade Clasifier setting look Opencv site.



#!/bin/bash
echo "Creating needed Files ..."
mkdir data info negImg negRawImg 

python resize_img.py

echo "Creating bg.txt file ..."
ls -d ./negImg/*.jpg | paste -sd "\n" - > bg.txt

echo "Creating Positive Images ..."
opencv_createsamples -img posImg/stm_logo_50x50.png -bg bg.txt -info ./info/info.lst -pngoutput info -maxxangle 0.1 -maxyangle 0.1 -maxzangle 0.1 -num 100

echo "Creating Positive Vector File ..."
opencv_createsamples -info info/info.lst -num 1189 -w 24 -h 24 -vec positives.vec

echo "Training Starting ..."
opencv_traincascade -data data -vec positives.vec -bg bg.txt -numPos 10 -numNeg 200 -numStages 20 -w 24 -h 24

import cv2 
import os
import numpy as np

def resize_img():
    pic_num = 1

    if not os.path.exists('negImg'):
        os.makedirs('negImg')
    print('Image Resizing ...')
    for filename in os.listdir('./negRawImg/'):
        parts = filename.split('.')
        if parts[-1] == 'png' or parts[-1] == 'jpg':
            img = cv2.imread('./negRawImg/'+filename)
            height, width, channels = img.shape 
            if height > 70 and width > 70:
                resized_img = cv2.resize(img, (100, 100))
                cv2.imwrite("negImg/"+str(pic_num)+".jpg", resized_img)
                pic_num = pic_num + 1
        else:
            print('it is not image')


resize_img() 
